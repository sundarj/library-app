(ns world.sometimes.nota.ui.devcards.editor
  (:require
   [devcards.core :refer [defcard]]
   [fulcro.client.cards :refer [defcard-fulcro]]
   [fulcro.client.primitives :as prim]
   [world.sometimes.nota.ui.editor :as editor]))

#_(defcard FormMetadata
  "The part of the form corresponding to the active note's metadata."
  (fn [*state _]
    (form-metadata/ui-form-metadata (deref *state)))
  (prim/get-initial-state form-metadata/FormMetadata nil)
  {:inspect-data true})

#_(defcard FormFields
  "The part of the form corresponding to the active note's fields."
  (fn [*state _]
    (form-fields/ui-form-fields (deref *state)))
  [(prim/get-initial-state
    form-fields/Field
    {:id (random-uuid)
     :type :text/plain
     :data ["Plain text field"]})
   (prim/get-initial-state
    form-fields/Field
    {:id (random-uuid)
     :type :text/rich
     :data ["Rich text field"]})
   (prim/get-initial-state
    form-fields/Field
    {:id (random-uuid)
     :type :link/general
     :data ["https://example.com"]})
   (prim/get-initial-state
    form-fields/Field
    {:id (random-uuid)
     :type :link/image
     :data ["https://clojure.org/images/clojure-logo-120b.png"]})]
    {:inspect-data true})

(defcard EditorForm
  "The form for the Editor, for editing the fields and metadata of the
  active note."
  (fn [*state _]
    (editor/ui-editor-form (deref *state)))
  (prim/get-initial-state editor/EditorForm nil)
  {:inspect-data true})

#_(defcard-fulcro Editor
  "The kernel of the app; all user interaction happens here. Has a collection of
  of working notes, as well as an active note (the note currently being
  edited). All of the fields of the notes, as well as their metadata can
  be revised using this component."
  editor/Editor
  {}
    {:inspect-data true})
