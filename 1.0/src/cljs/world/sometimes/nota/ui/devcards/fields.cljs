(ns world.sometimes.nota.ui.devcards.fields
  (:require
   [devcards.core :refer [defcard]]
   [fulcro.client.cards :refer [defcard-fulcro]]
   [fulcro.client.primitives :as prim]
   [world.sometimes.nota.ui.editor :as editor]))

(defcard text-plain
  (fn [*state _]
    (editor/ui-field (deref *state)))
  (prim/get-initial-state
   editor/Field
   {:id (random-uuid)
    :type :text/plain
    :data ["Plain text field"]})
  {:inspect-data true})

(defcard text-rich
  (fn [*state _]
    (editor/ui-field (deref *state)))
  (prim/get-initial-state
   editor/Field
   {:id (random-uuid)
    :type :text/rich
    :data ["Rich text field"]})
  {:inspect-data true})

(defcard link-general
  (fn [*state _]
    (editor/ui-field (deref *state)))
  (prim/get-initial-state
   editor/Field
   {:id (random-uuid)
    :type :link/general
    :data ["https://example.com"]})
  {:inspect-data true})

(defcard link-image
  (fn [*state _]
    (editor/ui-field (deref *state)))
  (prim/get-initial-state
   editor/Field
   {:id (random-uuid)
    :type :link/image
    :data ["https://clojure.org/images/clojure-logo-120b.png"]})
  {:inspect-data true})

