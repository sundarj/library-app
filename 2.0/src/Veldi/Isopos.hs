{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE DerivingStrategies #-}

module Veldi.Isopos (prerender) where

import Prelude hiding ((.))
import Control.Arrow ((<<<), (>>>))
import Data.Function ((&))
import qualified Data.Text as Text
import qualified Data.RDF as RDF

import Veldi.Isopos.Data (Renderable (..))

{-
'what should the render function do when it hits a collection'
ah, I think I see now
you don't want your render function to be recursive. you want to only render **once**, but you want a separate collection/aggregation of what **to** render

i.e. you don't render by printing each element and if you have a collection you call render for each element and so on
you render by first traversing the map of the wanted elements, and then you render *that*

in slightly longer, rather than

render :: _ -> m ()
render resource = case resource of
text -> print text
image -> print (getfilepath image)
collection -> mapM render collection

you would have

traverseResource :: _ -> Text
traverseResource resource = case resource of
text -> text
image -> getfilepath image
collection -> let recursedResources = map traverseResource collection in combineCollectionSomehowIntoText recursedResources

render :: _ -> m ()
render resource = print (traverseResource resource)

well, obviously `traverseResource` will probably be either monadic in some state/reader of the resources or have the whole map as a parameter to do the id lookups but it would not be (necessarily) in IO; i.e. it would not be printing anything

this also means you can post-process in the between stage

picking the most important (by some criteria) N items for instance

bonus bonus: it means you can write tests 🙂

------

What you need if you want views of everything that isn't collections is actually two view functions:
Your ordinary `view` function, and another function for viewing a resource when it is a child of a collection.
This function would check what type of resource it is and then call either `view` or `preview` on it.
You could call it `viewCollectionChild` or something

------

I recommend giving the user a way to backtrack as well
-}

prerender :: RDF.Triples -> Renderable
prerender triples =
  MkComposite $ map prerenderTriple triples
  where
    prerenderTriple (RDF.Triple _ _ obj) =  
      let obj' = 
            case obj of  
              RDF.UNode iri -> Text.unpack iri
              RDF.BNode _ -> "<nil>"  
              RDF.LNode val -> 
                case val of  
                  (RDF.PlainL text) -> Text.unpack text
                  (RDF.PlainLL text _lang) ->
                    Text.unpack text  
                  _ -> "<invalid>"  
              _ -> "<invalid>"
      in MkScalar obj'
