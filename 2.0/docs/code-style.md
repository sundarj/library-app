# Code style guide
“Programs must be written for people to read, and only incidentally for machines to execute.”
― Harold Abelson, Structure and Interpretation of Computer Programs
## Settings
- Column width: 50 (to fit on my iPhone SE)
- Use spaces for indentation and alignment, not
  tabs
- 2-space indent
## Functions
### Application and composition
Never use `.`. Use `<<<` or `>>>` from Control.Arrow instead.

```hs
-- Good
f <<< g
g >>> f

-- Bad
f . g
```

Only use `$` and `<<<` in expressions that fit on
one line (so that you read it linearly rather
than recursively).

```hs
-- Good:
f $ g $ h $ x
x & h & g & f
map (f <<< g <<< h) xs
map (h >>> g >>> f) xs

-- Bad:
someLongFunction
$ someLongGunction
$ someLongHunction
$ x

map (someLongFunction
     <<< someLongGunction
     <<< someLongHunction) xs```

For expressions that you want to spread across
multiple lines, use either `>>>`or `&`.

```hs
-- Good
x
& someLongFunction
& someLongGunction
& someLongHunction

map (someLongFunction
     >>> someLongGunction
     >>> someLongHunction) xs```
- 
