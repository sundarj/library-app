"use strict";

(function () {
  const selectOne = selector => document.querySelector(selector);
  const selectAll = selector => document.querySelectorAll(selector);

  const editButtons = selectAll(".c-resource__edit-btn");
  
  if (editButtons.length > 0) {
    for (let btn of editButtons) {
      btn.addEventListener("click", function (event) {
        const icon = btn.querySelector(".material-symbols-outlined");
        const state = icon.textContent === "edit" ? "pre-edit" : "editing";
        icon.textContent = state === "editing" ? "check" : "edit";
        if (state === "pre-edit") {
          const tripRaw = btn.parentElement.parentElement.dataset.triple;
          const trip = JSON.parse(tripRaw.replace("&quot;", '"'));
          alert(trip);
        } else {
        }
      }
    }
  }
})();
