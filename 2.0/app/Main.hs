module Main (main) where

import Prelude hiding ((.))
import Control.Arrow ((<<<), (>>>))
import Data.Function ((&))
import qualified Data.List as List
import qualified Data.RDF as RDF
import qualified Data.RDF.Query as RDFQ
import qualified Text.Read as Read
import Control.Monad (forM_)
import qualified System.IO as SIO

import Isopos (  prerender
               , Renderable(..)
               , DB
               , title
               , description
               , seeAlso
               , selfref
               , text
               )

import qualified Tui as Tui
import qualified Web as Web

main :: IO ()
main = Web.main
